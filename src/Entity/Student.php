<?php

namespace App\Entity;

use App\Repository\StudentRepository;
use App\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=StudentRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class Student
{

    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ClassRoom", inversedBy="students")
     * @ORM\JoinColumn(name="class_room_id", referencedColumnName="id")
     */
    private $classRooms;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @Assert\NotBlank
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @Assert\NotBlank
     */
    private $firstName;

     /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\NotBlank
     */
    private $birthday;

     /**
     * @ORM\Column(type="string", length=7, nullable=true)
     * @Assert\NotBlank
     */
    private $cin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClassRoomCode(): ?string
    {
        return $this->classRoomCode;
    }

    public function setClassRoomCode(string $classRoomCode): self
    {
        $this->classRoomCode = $classRoomCode;

        return $this;
    }

    public function getSpeciality(): ?string
    {
        return $this->speciality;
    }

    public function setSpeciality(?string $speciality): self
    {
        $this->speciality = $speciality;

        return $this;
    }

    public function getNumberRoom(): ?string
    {
        return $this->numberRoom;
    }

    public function setNumberRoom(?string $numberRoom): self
    {
        $this->numberRoom = $numberRoom;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getCin(): ?string
    {
        return $this->cin;
    }

    public function setCin(?string $cin): self
    {
        $this->cin = $cin;

        return $this;
    }

    public function getClassRooms(): ?ClassRoom
    {
        return $this->classRooms;
    }

    public function setClassRooms(?ClassRoom $classRooms): self
    {
        $this->classRooms = $classRooms;

        return $this;
    }

}
