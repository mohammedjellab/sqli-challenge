<?php

namespace App\Entity;

use App\Repository\ClassRoomRepository;
use App\Traits\Timestampable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=ClassRoomRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class ClassRoom
{

    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5, unique=true)
     * @Assert\NotBlank
     */
    private $classRoomCode;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @Assert\NotBlank
     */
    private $speciality;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     * @Assert\NotBlank
     */
    private $numberRoom;

    /**
     * @ORM\OneToMany(targetEntity="Student", mappedBy="classRooms")
     */
    private $students;

    public function __construct()
    {
        $this->students = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClassRoomCode(): ?string
    {
        return $this->classRoomCode;
    }

    public function setClassRoomCode(string $classRoomCode): self
    {
        $this->classRoomCode = $classRoomCode;

        return $this;
    }

    public function getSpeciality(): ?string
    {
        return $this->speciality;
    }

    public function setSpeciality(?string $speciality): self
    {
        $this->speciality = $speciality;

        return $this;
    }

    public function getNumberRoom(): ?string
    {
        return $this->numberRoom;
    }

    public function setNumberRoom(?string $numberRoom): self
    {
        $this->numberRoom = $numberRoom;

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->setClassRooms($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->students->removeElement($student)) {
            // set the owning side to null (unless already changed)
            if ($student->getClassRooms() === $this) {
                $student->setClassRooms(null);
            }
        }

        return $this;
    }

}
