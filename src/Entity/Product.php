<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use App\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class Product
{

    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotBlank
     */
    private $libelle;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="product_zip", fileNameProperty="zipName")
     * @Assert\NotBlank
     * @Assert\File(
     *  maxSize="1024M",
     *  mimeTypes={"application/zip", "application/octet-stream", "application/x-zip-compressed", "multipart/x-zip", "application/x-rar-compressed"},
     *  mimeTypesMessage = "Please upload a valid Zip File"
     * )
     * 
     * @var File|null
     */
    // mimeType={"application/zip", "application/octet-stream", "application/x-zip-compressed", "multipart/x-zip"}
    private $zipFile;

    /**
     * @ORM\Column(type="string")
     *
     * @var string|null
     */
    private $zipName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $zipFile
     */
    public function setZipFile(?File $zipFile = null): void
    {
        $this->zipFile = $zipFile;

        if (null !== $zipFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getZipFile(): ?File
    {
        return $this->zipFile;
    }

    public function setZipName(?string $zipName): void
    {
        $this->zipName = $zipName;
    }

    public function getZipName(): ?string
    {
        return $this->zipName;
    }
}
