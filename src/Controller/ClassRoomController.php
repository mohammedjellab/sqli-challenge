<?php

namespace App\Controller;

use App\Entity\ClassRoom;
use App\Form\ClassRoomType;
use App\Repository\ClassRoomRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/classroom")
 */
class ClassRoomController extends AbstractController
{
    /**
     * @Route("/", name="class_room_index", methods={"GET"})
     */
    public function index(ClassRoomRepository $classRoomRepository): Response
    {
        return $this->render('class_room/index.html.twig', [
            'class_rooms' => $classRoomRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="class_room_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $classRoom = new ClassRoom();
        $form = $this->createForm(ClassRoomType::class, $classRoom);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($classRoom);
            $entityManager->flush();

            return $this->redirectToRoute('class_room_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('class_room/new.html.twig', [
            'class_room' => $classRoom,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="class_room_show", methods={"GET"})
     */
    public function show(ClassRoom $classRoom): Response
    {
        return $this->render('class_room/show.html.twig', [
            'class_room' => $classRoom,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="class_room_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, ClassRoom $classRoom, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ClassRoomType::class, $classRoom);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('class_room_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('class_room/edit.html.twig', [
            'class_room' => $classRoom,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="class_room_delete", methods={"POST"})
     */
    public function delete(Request $request, ClassRoom $classRoom, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$classRoom->getId(), $request->request->get('_token'))) {
            $entityManager->remove($classRoom);
            $entityManager->flush();
        }

        return $this->redirectToRoute('class_room_index', [], Response::HTTP_SEE_OTHER);
    }
}
